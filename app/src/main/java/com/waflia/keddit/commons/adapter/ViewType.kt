package com.waflia.keddit.commons.adapter

interface ViewType{
    fun getViewType(): Int
}